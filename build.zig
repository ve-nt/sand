const std = @import("std");

const prog_name: []const u8 = "sand";

pub fn build(b: *std.build.Builder) anyerror!void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable(prog_name, "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.setOutputDir("bin");
    exe.install();
    for (packages) |pkg| exe.addPackage(pkg);
    exe.addBuildOption([]const u8, "prog_name", prog_name);
    exe.addBuildOption([]const u8, "version", versionString(b) catch unreachable);

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| run_cmd.addArgs(args);

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}

fn versionString(b: *std.build.Builder) ![]const u8 {
    var code: u8 = undefined;
    const git_tag = try b.execAllowFail(
        &.{ "git", "describe" },
        &code,
        std.ChildProcess.StdIo.Ignore,
    );
    const version = b.fmt("{s} ({s}-{s})", .{
        git_tag[0 .. git_tag.len - 1],
        @tagName(std.builtin.cpu.arch),
        @tagName(std.builtin.os.tag),
    });
    return version;
}

const packages = [_]std.build.Pkg{
    std.build.Pkg{
        .name = "clap",
        .path = "lib/clap/clap.zig",
    },
};
