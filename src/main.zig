const std = @import("std");
const clap = @import("clap");
const build_options = @import("build_options");
const param = @import("param.zig");
const util = @import("util.zig");

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(!gpa.deinit()); // Assert no memory leaks.
    var allocator = &gpa.allocator;

    const stdout = std.io.getStdOut().writer();
    const stderr = std.io.getStdOut().writer();

    var args = param.parseArgs(allocator, stderr) catch |_| help(stdout);
    defer args.deinit();

    if (args.flag("--help")) help(stderr);
    if (args.flag("--version")) version(stdout);
}

fn help(writer: anytype) noreturn {
    writer.print("Usage: {s} ", .{std.os.argv[0]}) catch unreachable;
    clap.usage(writer, &param.params) catch unreachable;
    writer.print("\nFlags: \n", .{}) catch unreachable;
    clap.help(writer, &param.params) catch unreachable;
    std.process.exit(@enumToInt(util.ExitCode.Failure));
}

fn version(writer: anytype) noreturn {
    writer.print("{s} {s}\n", .{
        build_options.prog_name,
        build_options.version,
    }) catch unreachable;
    std.process.exit(@enumToInt(util.ExitCode.Success));
}
