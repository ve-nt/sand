const std = @import("std");
const clap = @import("clap");
const Allocator = std.mem.Allocator;

fn param(line: []const u8) comptime clap.Param(clap.Help) {
    return clap.parseParam(line) catch unreachable;
}

pub const params = [_]clap.Param(clap.Help){
    param("-h, --help Display this help and exit"),
    param("-v, --version Display version information and exit"),
};

pub fn parseArgs(allocator: *Allocator, writer: anytype) !clap.Args(clap.Help, &params) {
    var diag: clap.Diagnostic = undefined;
    var args = clap.parse(
        clap.Help,
        &params,
        .{ .allocator = allocator, .diagnostic = &diag },
    ) catch |err| {
        diag.report(writer, err) catch unreachable;
        return err;
    };

    return args;
}
