pub const ExitCode = enum(u8) {
    Success = 0,
    Failure = 1,
};
